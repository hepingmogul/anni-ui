class Follow {
    constructor(trigger, target, options) {
        this.trigger = trigger;
        this.target = target;
        this.options = {
            offset: 2,
            placement: 'bottom',
            ...options,
        };
        this.calc();
    }

    calc() {
        const { offset, placement } = this.options;
        const ww = window.innerWidth;
        const wh = window.innerHeight;
        const triggerRect = this.trigger.getBoundingClientRect();
        const targetRect = this.target.getBoundingClientRect();
        const targetW = targetRect.width;
        const targetH = targetRect.height;
        const offsetTargetW = targetW + offset;
        const offsetTargetH = targetH + offset;

        const placements = [];
        if (triggerRect.top > offsetTargetH) {
            const top = triggerRect.top - offsetTargetH;
            if (ww - triggerRect.left > targetW) {
                placements.push({
                    placement: 'top-start',
                    css: {
                        top,
                        left: triggerRect.left,
                    }
                });
            }
            if (triggerRect.width / 2 + triggerRect.left > targetW / 2
                && ww - (triggerRect.width / 2 + triggerRect.left > targetW / 2)) {
                placements.push({
                    placement: 'top',
                    css: {
                        top,
                        left: (triggerRect.left + triggerRect.width / 2) - targetW / 2,
                    }
                });
            }
            if (triggerRect.right > targetW) {
                placements.push({
                    placement: 'top-end',
                    css: {
                        top,
                        left: triggerRect.right - targetW
                    }
                });
            }
        }

        if (ww - triggerRect.right > offsetTargetW) {
            const left = triggerRect.right + offset;
            if (wh - triggerRect.top > targetH) {
                placements.push({
                    placement: 'right-start',
                    css: {
                        top: triggerRect.top,
                        left,
                    }
                });
            }
            if (triggerRect.height / 2 + triggerRect.top > targetH / 2
                && wh - (triggerRect.height / 2 + triggerRect.top) > targetH / 2) {
                placements.push({
                    placement: 'right',
                    css: {
                        top: triggerRect.height / 2 + triggerRect.top - targetH / 2,
                        left,
                    }
                });
            }
            if (triggerRect.bottom > targetH) {
                placements.push({
                    placement: 'right-end',
                    css: {
                        top: triggerRect.bottom - targetH,
                        left,
                    }
                });
            }
        }

        if (wh - triggerRect.bottom > offsetTargetH) {
            const top = triggerRect.bottom + offset;
            if (ww - triggerRect.left > targetW) {
                placements.push({
                    placement: 'bottom-start',
                    css: {
                        top,
                        left: triggerRect.left,
                    }
                });
            }
            if (triggerRect.width / 2 + triggerRect.left > targetW / 2
                && ww - (triggerRect.width / 2 + triggerRect.left > targetW / 2)) {
                placements.push({
                    placement: 'bottom',
                    css: {
                        top,
                        left: (triggerRect.left + triggerRect.width / 2) - targetW / 2,
                    }
                });
            }
            if (triggerRect.right > targetW) {
                placements.push({
                    placement: 'bottom-end',
                    css: {
                        top,
                        left: triggerRect.right - targetW,
                    }
                });
            }
        }

        if (triggerRect.left > offsetTargetW) {
            const left = triggerRect.left - offsetTargetW;
            if (wh - triggerRect.top > targetH) {
                placements.push({
                    placement: 'left-start',
                    css: {
                        top: triggerRect.top,
                        left,
                    }
                });
            }
            if (triggerRect.height / 2 + triggerRect.top > targetH / 2
                && wh - (triggerRect.height / 2 + triggerRect.top) > targetH / 2) {
                placements.push({
                    placement: 'left',
                    css: {
                        top: triggerRect.height / 2 + triggerRect.top - targetH / 2,
                        left,
                    }
                });
            }
            if (triggerRect.bottom > targetH) {
                placements.push({
                    placement: 'left-end',
                    css: {
                        top: triggerRect.bottom - targetH,
                        left,
                    }
                });
            }
        }
        const last = placements.find(o => o.placement === this.placement) || placements[0];
        console.log(last);
        // this.container.dataset.placement = last.placement;
        // for (const key in last.css) {
        //     if (['top', 'left', 'right', 'bottom'].includes(key)) {
        //         last.css[key] = `${last.css[key]}px`
        //     }
        // }
        // setCss(this.container, last.css);
    }
}

export default Follow;
