import gulp from 'gulp';
import gulpSass from 'gulp-sass';
import * as sass from 'sass';
import cleanCss from 'gulp-clean-css';
import { rollup } from 'rollup';
import commonjs from '@rollup/plugin-commonjs';
import resolve from '@rollup/plugin-node-resolve';
import babel from '@rollup/plugin-babel';
import terser from '@rollup/plugin-terser';
import { envrc } from './envrc.js';
// https://www.npmjs.com/package/rollup-plugin-import-css
import css from "rollup-plugin-import-css";

const scss = gulpSass(sass);

const env = envrc[process.env.APP_MODE]
console.log(env);

const cssPaths = [
    'src/home/home.scss',
    'src/user/history.scss',
    'src/login/login.scss',
    'src/signup/signup.scss',
    'src/media-player/media-player.scss',
    'src/workspace/workspace.scss',
    'src/summary/summary.scss',
    'src/user/change-pwd.scss',
    'src/error-page/error-page.scss',
    'src/user/register-source.scss',
    'src/auth-reset-pwd/auth-reset-pwd.scss',
    'src/privacy-policy/privacy-policy.scss',
    'src/terms-service/terms-service.scss',
    'src/share/share.scss',
    'src/bilibili-summarizer/bilibili-summarizer.scss',
    'src/youtube-summarizer/youtube-summarizer.scss',
];
const jsPaths = [
    'src/home/home.js',
    'src/user/history.js',
    'src/login/login.js',
    'src/signup/signup.js',
    'src/media-player/media-player.js',
    'src/workspace/workspace.js',
    'src/summary/summary.js',
    'src/user/change-pwd.js',
    'src/user/register-source.js',
    'src/auth-reset-pwd/auth-reset-pwd.js',
    'src/share/share.js',
    'src/bilibili-summarizer/bilibili-summarizer.js',
    'src/youtube-summarizer/youtube-summarizer.js',
];

function styles() {
    let next = gulp.src(cssPaths);
    next = next.pipe(scss().on('error', scss.logError));
    if (env.compress) {
        next = next.pipe(cleanCss());
    }
    next = next.pipe(gulp.dest(env.output.cssPath));
    return next;
}

function rollupBuild() {
    const plugins = [
        resolve(),
        commonjs(),
        css(),
        babel({
            presets: ['@babel/preset-env'],
            exclude: 'node_modules/**',
            babelHelpers: 'bundled',
        }),
    ];
    if (env.compress) {
        plugins.push(terser());
    }
    const config = {
        input: jsPaths,
        output: {
            dir: env.output.jsPath,
            format: 'es',
            entryFileNames: '[name].js',
            sourcemap: env.sourcemap,
        },
        plugins
    };
    let next = rollup(config);
    next = next.then(bundle => bundle.write(config.output));
    return next;
}

function scripts(done) {
    rollupBuild().then(() => {
        done();
    }).catch((e) => {
        console.error(e);
        done();
    });
}


function watch() {
    gulp.watch(['src/**/*.scss'], styles);
    gulp.watch(['src/**/*.js'], scripts);
}

export {
    styles,
    scripts,
    watch,
}

const build = gulp.parallel(
    styles,
    scripts,
);

const dev = gulp.series(build, watch);

const only = env.mode === 'local' ? dev : build

export default only;
