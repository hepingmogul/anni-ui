export const envrc = {
    local: {
        mode: 'local',
        compress: false,
        sourcemap: true,
        output: {
            jsPath: `../web/res/local/js`,
            cssPath: `../web/res/local/css`,
        }
    },
    test: {
        mode: 'test',
        compress: false,
        sourcemap: false,
        output: {
            jsPath: `../web/res/test/js`,
            cssPath: `../web/res/test/css`,
        }
    },
    prod: {
        mode: 'prod',
        compress: true,
        sourcemap: true,
        output: {
            jsPath: `../web/res/prod/js`,
            cssPath: `../web/res/prod/css`,
        }
    }
};